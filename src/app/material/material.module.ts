import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MdSidenavModule, MdMenuModule, MdToolbarModule, MdIconModule} from '@angular/material';

import { GymHeaderComponent } from '../Gym/gym-header/gym-header.component';
import { GymFooterComponent } from '../Gym/gym-footer/gym-footer.component';
import { GymBodyComponent } from '../Gym/gym-body/gym-body.component';

@NgModule({
  imports: [
    CommonModule,
    MdSidenavModule,
    MdMenuModule,
    MdToolbarModule,
    MdIconModule
  ],
  declarations: [
    GymHeaderComponent,
    GymFooterComponent,
    GymBodyComponent,]
})
export class MaterialModule { }

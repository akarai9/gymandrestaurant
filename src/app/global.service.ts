import { Injectable } from '@angular/core';
import { MdSnackBar} from '@angular/material';
import { AuthService } from "angular2-social-login";


@Injectable()
export class GlobalService {

  public user;
  sub: any;	

  constructor(public snackBar: MdSnackBar,public _auth:AuthService,) { }

 /* openSnackBar() {
    this.snackBar.openFromComponent(GymRoasterComponent, {
      duration: 500,
    });
  }  */
  
  signIn(provider){
    this.sub = this._auth.login(provider).subscribe(
      (data) => {
        console.log(data);
        this.user=data;}
    )
  }

}

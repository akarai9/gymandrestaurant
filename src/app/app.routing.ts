import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { GymHeaderComponent } from './Gym/gym-header/gym-header.component';
import { GymFooterComponent } from './Gym/gym-footer/gym-footer.component';
import { GymBodyComponent } from './Gym/gym-body/gym-body.component';
import { GymRoasterComponent } from './Gym/gym-roaster/gym-roaster.component';
import { GymScheduleComponent } from './Gym/gym-schedule/gym-schedule.component';
import { GymDelegateComponent } from './Gym/gym-delegate/gym-delegate.component';
import { GymVotingComponent } from './Gym/gym-voting/gym-voting.component';
import { GymTabComponent } from './Gym/gym-tab/gym-tab.component';
// import { HomeComponent } from './restaurant/home/home.component';
import { RestaurantRoasterComponent } from './restaurant/restaurant-roaster/restaurant-roaster.component';
import { RestaurantDelegateComponent } from './restaurant/restaurant-delegate/restaurant-delegate.component';
import { RestaurantVotingComponent } from './restaurant/restaurant-voting/restaurant-voting.component';
import { RestaurantScheduleComponent } from './restaurant/restaurant-schedule/restaurant-schedule.component';
import { RestaurantBoxComponent } from './restaurant/restaurant-box/restaurant-box.component';
import { testerResolver } from './auth-gaurd.service'; 

const initialRoute = () => {
    return true;
};

const appRoutes: Routes = [
	{ path: '', redirectTo: 'voting', pathMatch: 'full'},
	{ path: 'home', component: GymBodyComponent },
	{ path: 'schedule', component: GymScheduleComponent},
	{ path: 'roaster', component: GymRoasterComponent}, 
	{ path: 'delegate', component: GymDelegateComponent},
	{ path: 'voting', component: GymVotingComponent, resolve:[testerResolver]},
	{ path: 'tab', component: GymTabComponent},
	{ path: 'lazyhome', loadChildren: 'app/restaurant/home/home.module#HomeModule' }
/*	{ path: 'home', component: HomeComponent },
	{ path: 'roaster', component: RestaurantRoasterComponent },
	{ path: 'schedule', component: RestaurantScheduleComponent},
	{ path: 'delegate', component: RestaurantDelegateComponent},
	{ path: 'voting', component: RestaurantVotingComponent},
	{ path: 'box', component: RestaurantBoxComponent},*/

]

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes,);
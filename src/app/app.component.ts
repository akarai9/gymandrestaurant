import { Component,HostBinding  } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { slideInDownAnimation} from './animations';
import { Route } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  /*  template: `
  <ul>
      <li *ngFor="let hero of heroes"
          [@heroState]="hero.state">
        {{hero}}
      </li>
    </ul>`,*/
  styleUrls: ['./app.component.css'],
  animations: [
    trigger('heroState', [
      state('inactive', style({
        backgroundColor: 'red',
        transform: 'scale(1)'
      })),
      state('active',   style({
        backgroundColor: '#cfd8dc',
        transform: 'scale(1.1)'
      })),
      transition('inactive => active', animate('100ms ease-in')),
      transition('active => inactive', animate('100ms ease-out'))
    ])
  ]
  // animations: [ slideInDownAnimation ]
})
export class AppComponent {

  
 heroes: Array<any> =  ["ram","shyam","shiv","Ganesh"]
// @HostBinding('@routeAnimation') routeAnimation = true;
/*@HostBinding('style.display')   display = 'block';
@HostBinding('style.position')  position = 'absolute';*/

  title = 'app';

  constructor(){ }

  
}
/*],
  
})*/

import { Component, OnInit } from '@angular/core';
import { Pipe, PipeTransform } from '@angular/core';

@Component({
  selector: 'app-gym-voting',
  templateUrl: './gym-voting.component.html',
  styleUrls: ['./gym-voting.component.css']
})
export class GymVotingComponent implements OnInit {


   favoriteSeason: string;

  seasons = [
    'John',
    'Kate',
    'Mary',
    'James',
  ];
  	
  constructor() { }

  ngOnInit() {
  }

}

/*
 * Raise the value exponentially
 * Takes an exponent argument that defaults to 1.
 * Usage:
 *   value | exponentialStrength:exponent
 * Example:
 *   {{ 2 | exponentialStrength:10 }}
 *   formats to: 1024
*/
@Pipe({name: 'exponentialStrength'})
export class ExponentialStrengthPipe implements PipeTransform {
  transform(value: number, exponent: string): number {
    let exp = parseFloat(exponent);
    return Math.pow(value, isNaN(exp) ? 1 : exp);
  }
}

@Pipe({name: 'uppercase1'})
export class UpperCasePipe implements PipeTransform {
  transform(value: string): string {
    return value.toUpperCase();
  }
}
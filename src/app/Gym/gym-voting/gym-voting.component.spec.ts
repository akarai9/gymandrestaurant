import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GymVotingComponent } from './gym-voting.component';

describe('GymVotingComponent', () => {
  let component: GymVotingComponent;
  let fixture: ComponentFixture<GymVotingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GymVotingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GymVotingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});

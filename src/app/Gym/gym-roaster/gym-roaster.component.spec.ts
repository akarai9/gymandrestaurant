import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GymRoasterComponent } from './gym-roaster.component';

describe('GymRoasterComponent', () => {
  let component: GymRoasterComponent;
  let fixture: ComponentFixture<GymRoasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GymRoasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GymRoasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});

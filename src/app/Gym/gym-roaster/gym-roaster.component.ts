import { Component, OnInit } from '@angular/core';
import { MdSnackBar} from '@angular/material';
import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';

import { GlobalService } from '../../global.service';

@Component({
  selector: 'app-gym-roaster',
  templateUrl: './gym-roaster.component.html',
  styleUrls: ['./gym-roaster.component.css'],
  animations: [
  trigger('heroState', [
    state('inactive', style({
      backgroundColor: '#eee',
      transform: 'scale(1)'
    })),
    state('active',   style({
      backgroundColor: '#cfd8dc',
      transform: 'scale(1.1)'
    })),
    transition('inactive => active', animate('100ms ease-in')),
    transition('active => inactive', animate('100ms ease-out'))
  ])
]
})
export class GymRoasterComponent implements OnInit {

  testing:boolean =false;

  constructor(public snackBar: MdSnackBar,public _globalservice:GlobalService) { }

  
  
  ngOnInit(){}

  openSnackBar(message: string, action: string){
    this.snackBar.open(message,action,{
      duration: 2000,
    });
  }

  numbers:Array<any> = [0,1,2,3,4,5,6,7,8,8,9,9,8,7]

   signInSocial(provider){
        this._globalservice.signIn(provider);
        this.openSnackBar(provider,'');
  }

}

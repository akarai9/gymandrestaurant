import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GymBodyComponent } from './gym-body.component';

describe('GymBodyComponent', () => {
  let component: GymBodyComponent;
  let fixture: ComponentFixture<GymBodyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GymBodyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GymBodyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});

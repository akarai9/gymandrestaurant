import { Component, OnInit } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

@Component({
  selector: 'app-gym-body',
  templateUrl: './gym-body.component.html',
  styleUrls: ['./gym-body.component.css']
})

export class GymBodyComponent implements OnInit {

 images: string[];
    config: any = {
        pagination: '.swiper-pagination',
        paginationClickable: true,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        spaceBetween: 30
    };
  

  constructor() { }

  ngOnInit() {
    this.loadImages();
  
  }
      loadImages() {
        this.images = [
            '/assets/Images/gym1.jpg',
            '/assets/Images/gym3.jpg',
            '/assets/Images/gym.jpg'
        ];
    }
}

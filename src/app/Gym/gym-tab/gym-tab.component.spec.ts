import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GymTabComponent } from './gym-tab.component';

describe('GymTabComponent', () => {
  let component: GymTabComponent;
  let fixture: ComponentFixture<GymTabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GymTabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GymTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});

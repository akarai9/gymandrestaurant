import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GymFooterComponent } from './gym-footer.component';

describe('GymFooterComponent', () => {
  let component: GymFooterComponent;
  let fixture: ComponentFixture<GymFooterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GymFooterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GymFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});

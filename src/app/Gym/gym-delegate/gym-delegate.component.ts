import { Component, OnInit } from '@angular/core';
import { MdSnackBar} from '@angular/material';

import { GlobalService } from '../../global.service';

@Component({
  selector: 'app-gym-delegate',
  templateUrl: './gym-delegate.component.html',
  styleUrls: ['./gym-delegate.component.css']
})
export class GymDelegateComponent implements OnInit {

   lengtharray: Array<any> = [1,2];

  constructor(public snackBar: MdSnackBar,public _globalservice:GlobalService) { }
  
  ngOnInit(){}

  openSnackBar(message: string, action: string){
    this.snackBar.open(message,action,{
      duration: 2000,
    });
  }

   signInSocial(provider){
        this._globalservice.signIn(provider);
        this.openSnackBar(provider,'');
  }

}

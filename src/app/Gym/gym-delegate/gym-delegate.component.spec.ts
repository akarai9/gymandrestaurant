import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GymDelegateComponent } from './gym-delegate.component';

describe('GymDelegateComponent', () => {
  let component: GymDelegateComponent;
  let fixture: ComponentFixture<GymDelegateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GymDelegateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GymDelegateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});

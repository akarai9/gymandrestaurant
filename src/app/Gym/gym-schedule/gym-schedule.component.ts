import { Component, OnInit } from '@angular/core';
import { MdSnackBar} from '@angular/material';

import { GlobalService } from '../../global.service';

@Component({
  selector: 'app-gym-schedule',
  templateUrl: './gym-schedule.component.html',
  styleUrls: ['./gym-schedule.component.css']
})
export class GymScheduleComponent implements OnInit {

  array: Array<any> = [1,2,3];
  today: number = Date.now();
   	
  constructor(public snackBar: MdSnackBar,public _globalservice:GlobalService) { }
  
  ngOnInit(){}

  openSnackBar(message: string, action: string){
    this.snackBar.open(message,action,{
      duration: 2000,
    });
  }

   signInSocial(provider){
        this._globalservice.signIn(provider);
        this.openSnackBar(provider,'');
  }



}

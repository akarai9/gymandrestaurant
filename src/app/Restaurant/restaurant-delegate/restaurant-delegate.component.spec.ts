import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RestaurantDelegateComponent } from './restaurant-delegate.component';

describe('RestaurantDelegateComponent', () => {
  let component: RestaurantDelegateComponent;
  let fixture: ComponentFixture<RestaurantDelegateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RestaurantDelegateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RestaurantDelegateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});

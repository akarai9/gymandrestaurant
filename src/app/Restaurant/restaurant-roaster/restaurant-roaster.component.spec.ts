import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RestaurantRoasterComponent } from './restaurant-roaster.component';

describe('RestaurantRoasterComponent', () => {
  let component: RestaurantRoasterComponent;
  let fixture: ComponentFixture<RestaurantRoasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RestaurantRoasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RestaurantRoasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

   images: string[];
    config: any = {
        pagination: '.swiper-pagination',
        paginationClickable: true,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        spaceBetween: 30
    };
  

  constructor() { }

  ngOnInit() {
    this.loadImages();
  
  }
      loadImages() {
        this.images = [
            '/assets/Images/restau3.jpg',
            '/assets/Images/restau2.jpg',
            '/assets/Images/restau1.jpg'
        ];
    }
}

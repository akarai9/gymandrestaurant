import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-restaurant-schedule',
  templateUrl: './restaurant-schedule.component.html',
  styleUrls: ['./restaurant-schedule.component.css']
})
export class RestaurantScheduleComponent implements OnInit {
  
  array: Array<any> = [1,2,3];
  today: number = Date.now();
   	
  constructor() { }

  ngOnInit() {
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RestaurantVotingComponent } from './restaurant-voting.component';

describe('RestaurantVotingComponent', () => {
  let component: RestaurantVotingComponent;
  let fixture: ComponentFixture<RestaurantVotingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RestaurantVotingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RestaurantVotingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});

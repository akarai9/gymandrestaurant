import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-restaurant-voting',
  templateUrl: './restaurant-voting.component.html',
  styleUrls: ['./restaurant-voting.component.css']
})
export class RestaurantVotingComponent implements OnInit {
 
  favoriteSeason: string;

  seasons = [
    'Taj',
    'Dominos',
    'KFC',
    'Pizza Hut',
  ];

  constructor() { }

  ngOnInit() {
  }

}

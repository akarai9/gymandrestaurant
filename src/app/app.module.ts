import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { DatePipe } from '@angular/common';
import { BrowserAnimationsModule,NoopAnimationsModule } from '@angular/platform-browser/animations';
import { trigger, state, style, transition, animate } from '@angular/animations';

import { FlexLayoutModule } from '@angular/flex-layout';

import 'hammerjs';
import {CalendarModule} from 'primeng/primeng';
import * as $ from 'jquery';

import { MdSidenavModule, MdMenuModule, MdToolbarModule, MdIconModule, MdIconRegistry,
MdButtonModule, MdCardModule, MdInputModule, MdRadioModule, MdSnackBarModule, MdTabsModule
,MdProgressSpinnerModule, MdTableModule} from '@angular/material';
import {CdkTableModule} from '@angular/cdk';


import { SwiperModule } from 'angular2-useful-swiper';

import { GlobalService } from './global.service';
import { AppComponent } from './app.component';
import { routing} from './app.routing';
import { GymHeaderComponent } from './Gym/gym-header/gym-header.component';
import { GymFooterComponent } from './Gym/gym-footer/gym-footer.component';
import { GymBodyComponent } from './Gym/gym-body/gym-body.component';
import { GymRoasterComponent } from './Gym/gym-roaster/gym-roaster.component';
import { GymScheduleComponent } from './Gym/gym-schedule/gym-schedule.component';
import { GymDelegateComponent } from './Gym/gym-delegate/gym-delegate.component';
import { GymVotingComponent } from './Gym/gym-voting/gym-voting.component';
import { ExponentialStrengthPipe,UpperCasePipe } from './Gym/gym-voting/gym-voting.component';
import { GymTabComponent } from './Gym/gym-tab/gym-tab.component';
// import { HomeComponent } from './restaurant/home/home.component';
import { RestaurantRoasterComponent } from './restaurant/restaurant-roaster/restaurant-roaster.component';
import { RestaurantDelegateComponent } from './restaurant/restaurant-delegate/restaurant-delegate.component';
import { RestaurantVotingComponent } from './restaurant/restaurant-voting/restaurant-voting.component';
import { RestaurantScheduleComponent } from './restaurant/restaurant-schedule/restaurant-schedule.component';
import { RestaurantBoxComponent } from './restaurant/restaurant-box/restaurant-box.component';
/*import { MaterialModule } from './material/ma
terial.module';
*/

import { Angular2SocialLoginModule } from "angular2-social-login";
import { AuthService } from "angular2-social-login";

import { testerResolver } from './auth-gaurd.service'
 
let providers = {
    "google": {
      "clientId": "773038776462-m78on9f14p500vrcicc98otovd3fjsi9.apps.googleusercontent.com"
    },
    "linkedin": {
      "clientId": "81n4nk2synaign"
    },
    "facebook": {
      "clientId": "1342436439189105",
      "apiVersion": "v2.10" 
    }
  };

@NgModule({
  declarations: [
    AppComponent,
    GymHeaderComponent,
    GymFooterComponent,
    GymBodyComponent,
    GymRoasterComponent,
    GymScheduleComponent,
    GymDelegateComponent,
    GymVotingComponent,
    ExponentialStrengthPipe,
    UpperCasePipe,
    RestaurantRoasterComponent,
    RestaurantDelegateComponent,
    RestaurantVotingComponent,
    RestaurantScheduleComponent,
    RestaurantBoxComponent,
    GymTabComponent,
  ],
  imports: [
    BrowserModule,
    FlexLayoutModule,
    SwiperModule,
    CalendarModule,
    routing,
    BrowserAnimationsModule,NoopAnimationsModule,
    MdSidenavModule, MdMenuModule, MdToolbarModule, MdIconModule,
    MdButtonModule, MdCardModule, MdInputModule, MdRadioModule, MdSnackBarModule, 
    MdTabsModule,MdProgressSpinnerModule, MdTableModule
  ],
  providers: [GlobalService, MdIconRegistry,AuthService,testerResolver],
  bootstrap: [AppComponent]
})
export class AppModule { }
Angular2SocialLoginModule.loadProvidersScripts(providers);
